# Changelog

## v5
## v4
## v3
## v2
## v1.5.0
## v1.4.0

## v1.3.0

 * 16.09.2022
 * features:
    * added config with option groups
    * exclude, excludeExcept, includeOnly options
    * webpack stats generator `npm run webpack`
    * added all graphviz layput exports with examples
    * improved logging
    * added v5 and v3 webpack input formats
    * extract modules from chunks
    * add edge and nodes coloring
    * stats output
    * added excludeNodeByMaxDepsCount option
    * max deps filtering, in direct and inverted direction
    * changelog
 * refactoring: 
    * readme, docs, examples
    * useless code removed
    * naming
    * removed unused code, vfs
    * reduce data types logic
    * grouping logic
    * stateless code
    * filtering deduped and moved into one place
 * fixed:
    * graphml raw nodes position
    * graphml edges id
    * filtering
    * legacy code issues

## v1.2.0

 * 31.08.2022 17:47:46 #f829b8b
 * readme
 * folder structure refactoring
 * graphml refactored
 * added doc examples
 * folder structure refactored
 * graphml examples

## v1.0.0

 * 31.08.2022 12:03:26 #143d0b9
 * code fixed and became runnable
 * fixed dot, deps, circular output
 * cytoscape data parser
 * viewer draft, hexo
 * readme, docs
 * xml libs added
 * package format
 * graphml added
 * graphviz rendered output works: dot, png, simplified dot
 * webpack bundler added

